<?php
/**
 * Template Name: Specials
 *
 * @package WordPress
 * @subpackage Jupter
 * @since USA River Cruises 1.0
 */
get_header();

$current_year = date('Y'); // get current year
$current_year_time = strtotime($current_year); // convert current year to time
$next_year = date('Y', strtotime('+1 year')); // get next year
$next_year_time = strtotime($next_year); // convert next year to time
$todays_date = date('F j, Y'); // get date in ACF output format
$todays_date_time = strtotime($todays_date); // convert date string to time

// Define Args
$args = array();

// Define Array for meta_query
$meta_queries = array();

// Specials
$meta_queries[] = array(
  'key' => 'sale_text',
  'value' => '',
  'compare' => '>',
);

// Specials Date
$special_query_date = date('Ymd', strtotime($todays_date));
$meta_queries[] = array(
  'key'     => 'sale_end_date',
  'value'   => $special_query_date,
  'compare' => '>=',
  'type'    => 'DATE',
);

$args['post_type'] = array('any');
// Create Meta Queries
if (!empty($meta_queries)){
  $args['meta_query'] = $meta_queries;
};

$usarc_search_query = new WP_Query($args);

wp_reset_query();

include(get_stylesheet_directory() . '/php-partials/usarc_cards.php');

get_footer();

?>
