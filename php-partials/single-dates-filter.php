    <div class="tm-card--dates">
      <p>
				<?php
			    $tm_dates = get_field('available_dates');
					$column_id = array();
					if($tm_dates):
					// assign row as key to get date values
			    foreach( $tm_dates as $key => $row ) {
			    		// get the dates
			        $thedate = $row['date'];
			        // convert the current item to time for sorting
			        $column_id[ $key ] = strtotime($thedate);
			    } // end foreach
			    // sort the array of dates
			    array_multisort( $column_id, SORT_ASC, $tm_dates );
			    // once sorted look for the 'hide day' option
			    // $index = 0;
			    foreach( $tm_dates as $key => $row ) {
			    		// get the option
			        $hide_days = $row['hide_days'];
			        // store the position if the remove day option is checked
			        if ($hide_days === 'yes') {
			        	$hidden_dates[] =  $key;
							}
							// $index++;
			    } // end foreach

			    $new_index = 0;
			    // manipulate all of the data
			    foreach( $tm_dates  as $row ){
			    	// create date string from date
						$tm_date = DateTime::createFromFormat('F j, Y', $row['date']);
						// convert string to time in desired format
						$tm_date_time = strtotime($tm_date->format('F j, Y'));
						// get year from date
						$tm_year = $tm_date->format('Y');
						// convert year to time
						$tm_year_time = strtotime($tm_year);

						// If current index matches index of 'hide days option'
						if (empty($hidden_dates)) {
							$formatted_date = $tm_date->format('F j, Y');
						}else if (in_array($new_index, $hidden_dates)) {
							$formatted_date = $tm_date->format('F Y');
		  		 	}else{
		  		 		$formatted_date = $tm_date->format('F j, Y');
		  		 	}

		  		 	$new_index++;

		  		 	// if its today or the future
	    		 	if ($tm_date_time >= $todays_date_time) {
	    		 				$dates_list[] = $formatted_date;
						}
			    }
			    // if there are dates output the data
					if(!empty($dates_list)):
						// max number of days to show
						$num_days_display = 6;
						// count how many dates there are
						$dates_count = count($dates_list);
						// split the max number days into array
						$sliced_array = array_slice($dates_list, 0, $num_days_display);
						// if we have more that max number days
						if ($dates_count > $num_days_display) {
							$more_dates = '<br><span id="card--more-dates">See ' . ($dates_count - $num_days_display) . ' more available dates</span>';
							// add remaining days to new array
							$hidden_days = array_slice($dates_list, $num_days_display);
						}
						else{
							// set empty value if we are under our maximum days
							$more_dates = '';
						}
			    		echo '<img src="/wp-content/uploads/calendar.svg" alt="Calendar" class="style-svg"><span class="tm-dates--list"><span class="tm--date">' .  implode('</span> | <span class="tm--date">', $sliced_array) . '</span></span>' . $more_dates;
			    		echo '<br><span id="card--hidden-dates"><span class="tm--date">';
			    		// output more dates if there are any
			    		if ($dates_count > $num_days_display) {
			    			echo implode('</span> | <span class="tm--date">', $hidden_days) . '</span></span>';
			    		}
			    		echo "</span>";
			    		// reset the arrays
			    		unset($dates_list);
			    		unset($dates_count);
							unset($sliced_array);
					// If the dates have expired
			    else:
						echo '<li style="background-image:url(/wp-content/uploads/calendar.svg)">Please call for information on future dates.</li>';
					endif;
					// If there are no dates at all
					else:
						echo '<li style="background-image:url(/wp-content/uploads/calendar.svg)">Please call for information on future dates.</li>';
				endif;

			?>
      </p>
    </div>
