<?php
  $base_url = get_bloginfo('url');
  $avail_dates = get_field('available_dates');
  
?>

<div class="mk-page-section self-hosted full_layout full-width-2 js-el js-master-row mk-in-viewport book-now" data-intro-effect="false" id="book-now">
  <div class="page-section-content vc_row-fluid mk-grid">
    <div class="mk-padding-wrapper">
      <div class="vc_col-sm-12 wpb_column column_container _ height-full" style="">
        <div class="wpb_row vc_inner vc_row vc_row-fluid attched-false">
          <div class="wpb_column vc_column_container vc_col-sm-2">
            <div class="vc_column-inner">
              <div class="wpb_wrapper"></div>
            </div>
          </div>
          <div id="book-now-card" class="wpb_column vc_column_container vc_col-sm-8 vc_col-has-fill">
            <div class="vc_column-inner vc_custom_1509745433417">
              <div class="wpb_wrapper">
                <h2 class="mk-fancy-title simple-style color-single" id="book-today">
                  <span>Book Today</span>
                </h2>
				  			<h2 class="mk-fancy-title simple-style color-single" id="book-now-title">
                  <?php the_title();?>

                </h2>
                <div class="clearboth"></div>
              </div>
              <?php 
                //Book now param
                $content_id = "cruise_id=".get_the_id();
                
                //Conditional link depending on trip type
                if (get_post_type(get_the_id()) == 'cruise') : ?>
                  <a class="cruise-select book-now" href="<?php echo $base_url ?>/book-now?<?php echo $content_id ?>">Book Now</a>
                <?php else : ?>
                  <a class="book-now" href="<?php echo $base_url ?>/request-information?referer=<?php echo esc_html( get_the_title() ); ?>">Book Now</a>
                <?php endif; 
              ?>
 
              <a class="more-info" href="<?php echo $base_url ?>/request-info?referer=<?php echo esc_html( get_the_title() ); ?>">Request More Info</a>
              <p class="call">or call <a href="tel:1-800-578-1479">800.578.1479</a></p>
            </div>
          </div>
          <div class="wpb_column vc_column_container vc_col-sm-2">
            <div class="vc_column-inner">
              <div class="wpb_wrapper"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearboth"></div>
  </div>
  <div class="clearboth"></div>
</div>
