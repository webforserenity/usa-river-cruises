<?php
$trip_itinerary = get_field( 'trip_itinerary' );
$map = get_field( 'map_upload' );
$gallery = get_field('tm_gallery', false, false);
$map_size = 'full'; // (thumbnail, medium, large, full or custom size)
$cabin_details = get_field( 'cabin_details' );
$ship = get_field('cruise_ship'); // ship the cruise will take place on
$day_counter = 1; // counter for itinerary
?>
<div class="mk-main-wrapper-holder main-tm-section">
  <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
    <div class="theme-content no-padding">
      <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false js-master-row mk-in-viewport">
        <div class="vc_col-sm-6 wpb_column column_container height-full" style="">
          <div>
            <div class="itinerary">
            	<?php
								if($trip_itinerary)
								{
									echo "<h3 id='title--itinerary'>Your Itinerary</h3>";
									echo '<ul id="list--itinerary">';

									foreach($trip_itinerary as $key => $itinerary)
									{
										$day_title = $itinerary['day_title'];
										$day_description = $itinerary['day_description'];
										if ($day_title === '' || $day_title === 'n/a') {
											// do nothing
										}else{
											echo '<p><span class="day--number"><span class="number--inner">' . $day_counter . '</span></span><span class="day--title">' . $itinerary['day_title'] . '</span><br><span class="day--description">' . $itinerary['day_description'] . '</span></p>';
										}
										$day_counter++;
									}

									echo '</ul>';
								}
							?>
              <div class="clearboth"></div>
            </div>
          </div>
        </div>
        <div class="vc_col-sm-6 wpb_column column_container _ height-full" style="">
        	<?php
        	if ($map && $gallery) {
        		echo "<h3 id='title--map-gallery'>Map & Gallery</h3>";
        	}
        	else if($gallery){
        		echo "<h3 id='title--map-gallery'>Gallery</h3>";
        	}
        	else if($map) {
        		echo "<h3 id='title--map-gallery'>Map</h3>";
        	}
        	if( $map ) {
        			echo '<div id="map-upload">';
							echo wp_get_attachment_image( $map, $map_size );
							echo "</div>";
					}
					if ($gallery) {
						echo '<div id="tm--gallery">';
						$gallery_shortcode = '[vc_gallery type="image_grid" ' . 'images="' . implode(',', $gallery) . '"]';
						echo do_shortcode( $gallery_shortcode );
						echo "</div>";
					}
					if ($cabin_details){
						?>
						<div id="available-cabins">
							<h3>Cabin Categories</h3>
							<table>
								<thead>
									<th>Cabin</th>
									<th>Single Price</th>
									<th>Double Price</th>
								</thead>
								<tbody>
									<?php
									foreach($cabin_details as $detail)
									{
										$single_price = $detail['cabin_single_price'];
										$double_price = $detail['cabin_double_price'];
										if($single_price !== ''){
											$single_price_formatted = number_format($single_price);
										}
										if($double_price !== ''){
											$double_price_formatted = number_format($double_price);
										}
										echo '<tr><td>' . $detail['cabin_name'] . '</td><td>';
										if($single_price === '' || $single_price_formatted == 0){
											echo '—</td><td>';
										}
										else{
											echo '$' . $single_price_formatted . '</td><td>';
										}
										if($double_price === '' || $double_price_formatted == 0){
											echo '—</td></tr>';
										}
										else{
											echo '$' . $double_price_formatted . ' pp</td></tr>';
										}
									}
									?>
								</tbody>
							</table>
						<p>
        			<img src="/wp-content/uploads/ship.svg" alt="Ship" class="style-svg">
	         		<?php
							if( $ship ): ?>
								<?php foreach( $ship as $s ): // variable must NOT be called $post (IMPORTANT) ?>
					    	<span class="text">Read more about the <a href="<?php echo get_permalink( $s->ID ); ?>"><?php echo get_the_title( $s->ID ); ?></a></span>
								<?php endforeach; ?>
							<?php endif; ?>
      			</p>
      			<p class="note">*Prices may vary due to seasonality</p>
						<?php
					}
        	?>
        	</div>
        </div>
      </div>
      <div class="clearboth"></div>
    </div>
    <div class="clearboth"></div>
  </div>
</div>
