<?php
// 	CRUISE DETAILS VARIABLES
$days = get_field( 'tm_length_days' ); // # of cruise days
$nights = $days - 1; // cruise days minus 1
$double_occupancy = get_field( 'double_occupancy' ); // double occupancy rate
$single_occupancy = get_field( 'single_occupancy' ); // single occupancy rate
$ship = get_field('cruise_ship'); // ship the cruise will take place on
// SALE INFORMATION VARIABLES
$sale_end_date = get_field( 'sale_end_date' ); // sale end date
$sale_end = strtotime($sale_end_date); // convert sale string to time
$sale_text = get_field( 'sale_text' ); // sale description
// VARIABLES FOR CRUISE DATES
$current_year = date('Y'); // get current year
$current_year_time = strtotime($current_year); // convert current year to time
$next_year = date('Y', strtotime('+1 year')); // get next year
$next_year_time = strtotime($next_year); // convert next year to time
$todays_date = date('F j, Y'); // get date in ACF output format
$todays_date_time = strtotime($todays_date); // convert date string to time

$travel_mode ='';
if(is_singular( 'cruise' )){
	$travel_mode = 'Cruise';
}
if(is_singular( 'train' )){
	$travel_mode = 'Train';
}
if(is_singular( 'combination' )){
	$travel_mode = 'Train/Cruise';
}
//Book now param
$content_id = "cruise_id=".get_the_id();
?>

<div class="tm-card--wrapper <?php echo strtolower( $travel_mode ) ?>">
  <div class="tm-card--header">
    <h3>
      <?php echo $travel_mode ?> Details
    </h3>
  </div>
  <div class="tm-card--inner">
		<?php if($days): ?>
		<div class="tm-card--days-nights">
      <p>
        <img src="/wp-content/uploads/clock.svg" alt="Clock" class="style-svg"><span class="text"></span><?php echo $days ?> Days &amp; <?php echo $nights ?> Nights</span>
      </p>
    </div>
		<?php endif; ?>
		<?php if($double_occupancy || $single_occupancy): ?>
		<div class="tm-card--passenger-rates">
      <p class="tm-card--double-occupancy">
        <?php echo ($double_occupancy ? '<img src="/wp-content/uploads/couple.svg" alt="Double Occupancy" class="style-svg"><span class="text"> from $' . number_format($double_occupancy) . ' pp/do*</span>' : ''); ?>
      </p>
      <p class="tm-card--single-occupancy">
         <?php echo ($single_occupancy ? '<img src="/wp-content/uploads/single.svg" alt="Single Occupancy" class="style-svg"><span class="text">from $' . number_format($single_occupancy) . ' pp</span>' : ''); ?>
      </p>
    </div>
		<?php endif; ?>
		<!-- Dates for Cruises -->
		<?php get_template_part('php-partials/single','dates-filter'); ?>
		<?php
			if(is_singular('cruise')):
		?>
		<div class="tm-card--ship">
      <p>
        <img src="/wp-content/uploads/ship.svg" alt="Ship" class="style-svg">
         <?php
						if( $ship ): ?>
							<?php foreach( $ship as $s ): // variable must NOT be called $post (IMPORTANT) ?>
								<span class="text"><a href="<?php echo get_permalink( $s->ID ); ?>"><?php echo get_the_title( $s->ID ); ?></a></span>
							<?php endforeach; ?>
						<?php endif; ?>
      </p>
    </div>
		<?php endif; ?>
		<!-- Train Information -->
		<?php if (is_singular( 'train' )) : ?>
			<?php if (have_rows( 'train_specific')) : ?>
				<?php while ( have_rows('train_specific' ) ) : the_row(); ?>
					<?php if ( get_row_layout() == 'train_specific' ) : ?>
						<?php if(get_sub_field( 'trains' )) : ?>
                <div class="tm-card--train">
						      <p>
						        <img src="/wp-content/uploads/train.svg" alt="Train" class="style-svg"><span class="text"><?php the_sub_field( 'trains' ); ?></span>
						      </p>
			   				</div>
            <?php endif;?>
					<?php endif; ?>
		  	<?php endwhile; ?>
		  <?php endif; ?>
		<?php endif; ?>
		<!-- End Train Information -->
		<?php
		if($sale_end):
       if ($sale_end >= $todays_date_time): ?>
				<div class="tm-card--sale">
					<p>
						<img src="/wp-content/uploads/star.svg" alt="Sale" class="style-svg">
						<?php echo $sale_text ?>
					</p>
				</div>
			 <?php endif; ?>
		<?php endif; ?>
		
		<?php 


			//Conditional link depending on trip type
			if (get_post_type(get_the_id()) == 'cruise') : ?>
				<a class="book-now" href="<?php echo $base_url ?>/book-now?<?php echo $content_id; ?>">Book Now</a>
			<?php else : ?>
				<a class="book-now" href="<?php echo $base_url ?>/request-information?referer=<?php echo esc_html( get_the_title() ); ?>">Book Now</a>

			<?php endif; 
		?>

    <a class="more-info" href="<?php echo $base_url ?>/request-info?referer=<?php echo esc_html( get_the_title() ); ?>">Request More Info</a>
		<p class="call">or call <a href="tel:1-800-578-1479">800.578.1479</a></p>
		<p class="note">*Per person/double occupancy. Special single rates apply where listed. Excludes port fees.
			<br/>Itineraries may operate in reverse.</p>
  </div>
</div>
