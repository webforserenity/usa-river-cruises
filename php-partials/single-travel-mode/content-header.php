<?php
// ACF FIELDS
$header_image = get_field( 'tm_header_image' );
$header_overlay = 'rgba(0,0,0,0.7)';
$description = get_field( 'tm_description' );
?>

<div class="mk-page-section-inner">
<div class="mk-video-color-mask" style="background-color:<?php echo $header_overlay ?>"></div>
<div class="background-layer-holder">
  <div class="stm-background-layer mk-background-stretch none-blend-effect js-el" id="header-background-layer" style="background-image: url(<?php echo wp_get_attachment_image_src($header_image, 'full')[0]; ?>);">
    <div class="mk-color-layer"></div>
  </div>
</div>
</div>
<div class="page-section-content vc_row-fluid page-section-fullwidth">
	<div class="mk-padding-wrapper">
	  <div class="vc_col-sm-12 wpb_column column_container _ height-full" style="">
	    <div class="wpb_row vc_inner vc_row vc_row-fluid attched-false">
	      <div class="mk-grid">
	        <div class="wpb_column vc_column_container vc_col-sm-6">
	          <div class="vc_column-inner">
	            <div class="wpb_wrapper">
	              <h1 class="mk-fancy-title simple-style color-single" id="tm--title">
	                <?php the_title() ?>
	              </h1>
	              <div class="clearboth"></div>
	              <div>
	                <div class="mk-text-block" id="tm--description">
	                  <p>
	                    <?php echo $description; ?>
	                  </p>
	                  <div class="clearboth"></div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="wpb_column vc_column_container vc_col-sm-6">
	          <div class="vc_column-inner">
	            <div class="wpb_wrapper">
	              <div class="mk-padding-divider clearfix" id="card-padding"></div>
	              <?php get_template_part('php-partials/single-travel-mode/content','card'); ?>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="clearboth"></div>
</div>
