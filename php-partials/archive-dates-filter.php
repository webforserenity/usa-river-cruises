<?php

$todays_date = date('F j, Y'); // get date in ACF output format
$todays_date_time = strtotime($todays_date); // convert date string to time
$tm_dates = get_field('available_dates');
$column_id = array();
if($tm_dates):
// assign row as key to get date values
foreach( $tm_dates as $key => $row ) {
		// get the dates
    $thedate = $row['date'];
    // convert the current item to time for sorting
    $column_id[ $key ] = strtotime($thedate);
} // end foreach
// sort the array of dates
array_multisort( $column_id, SORT_ASC, $tm_dates );

$new_index = 0;
// manipulate all of the data
foreach( $tm_dates  as $row ){
	// create date string from date
	$tm_date = DateTime::createFromFormat('F j, Y', $row['date']);
	// convert string to time in desired format
	$tm_date_time = strtotime($tm_date->format('F j, Y'));
	// get year from date
	$tm_year = $tm_date->format('Y');
	// convert year to time
	$tm_year_time = strtotime($tm_year);

	// If current index matches index of 'hide days option'
	if (empty($hidden_dates)) {
		$formatted_date = $tm_date->format('F j, Y');
	}else if (in_array($new_index, $hidden_dates)) {
		$formatted_date = $tm_date->format('F Y');
 	}else{
 		$formatted_date = $tm_date->format('F j, Y');
 	}

 	$new_index++;

 	// if its today or the future
 	if ($tm_date_time >= $todays_date_time) {
 				$dates_list[] = $formatted_date;
	}
}
// if there are dates output the data
if(!empty($dates_list)):
	// max number of days to show
	$num_days_display = 3;
	// count how many dates there are
	$dates_count = count($dates_list);
	// split the max number days into array
	$sliced_array = array_slice($dates_list, 0, $num_days_display);
	// if we have more that max number days
	if ($dates_count > $num_days_display) {
    $dates_display = $dates_count - 3;
    if ($dates_display < 2){
      $more_dates = '--<br/><a href="' . get_permalink() . '" style="font-size:0.83em">' .  ($dates_display) . ' more date available.</a>';
    }
    else {
      $more_dates = '--<br/><a href="' . get_permalink() . '" style="font-size:0.83em">' .  ($dates_display) . ' more dates available.</a>';
    }
	}
	else{
		// set empty value if we are under our maximum days
		$more_dates = '';
	}
		echo '<li style="background-image:url(/wp-content/uploads/calendar.svg)"><span class="tm-dates--list"><span class="cruise--date">' .  implode(',</span><span class="cruise--date">', $sliced_array) . '</span></span>' . $more_dates . '</li>';
    unset($dates_list);
    unset($dates_count);
    unset($sliced_array);
// If the dates have expired
else:
	echo '<li style="background-image:url(/wp-content/uploads/calendar.svg)">Please call for information on future dates.</li>';
endif;
// If there are no dates at all
else:
	echo '<li style="background-image:url(/wp-content/uploads/calendar.svg)">Please call for information on future dates.</li>';
endif;
