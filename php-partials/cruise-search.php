

    <div id="usarc-search">
      <div id="search-container">
        <div class="cruise-finder-title">
          <label for="cruise-finder-checkbox">Search for a Cruise. &nbsp; <img class="toggle-arrow" src="<?php echo get_stylesheet_directory_uri() ?>/img/select-down-red.svg" /></label>
        </div>
        <input type="checkbox" id="cruise-finder-checkbox" />
        <form class="cruise-finder-fields" method="get" action="/trip-search-results/">
          <select name="travel-type">
            <option value="">-- Travel Type --</option>
            <option value="cruise">Cruise</option>
            <option value="train">Train</option>
            <option value="cruise_train">Cruise + Train</option>
          </select>
          <select name="destination">
            <option value="">-- Cruise Destination --</option>
            <?php
              $_terms = get_terms( array('destination') );
              foreach ($_terms as $term) :
                echo '<option value="' . $term->slug .'">' . $term->name . '</option>';
              endforeach ;
            ?>
          </select>
          <select name="ships">
            <option value="">-- Cruise Ship --</option>
            <?php
              $args = array(
                'posts_per_page' => -1,
                'orderby'        => 'title',
                'order'          => 'ASC',
                'post_type'      => 'ship',
              );
              $_ships = get_posts( $args );
              foreach ($_ships as $ship) :
                echo '<option value="' . $ship->ID .'">' . $ship->post_title . '</option>';
              endforeach ;
            ?>
          </select>
          <select name="length">
            <option value="">-- Cruise Length --</option>
            <option value="5">5 days & under</option>
            <option value="10">10 days & under</option>
            <option value="15">15 days & under</option>
            <option value="20">20 days & under</option>
            ?>
          </select>
          <select name="price">
            <option value="">-- Max Price --</option>
            <option value="2500">$2,500</option>
            <option value="5000">$5,000</option>
            <option value="7500">$7,500</option>
            <option value="10000">$10,000</option>
            <option value="20000">$20,000</option>
          </select>
          <select name="month">
            <option value="">-- Month --</option>
            <option value="01">January</option>
            <option value="02">February</option>
            <option value="03">March</option>
            <option value="04">April</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">August</option>
            <option value="09">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select>
          <select name="cruise-year">
            <option value="">-- Year --</option>
            <?php
              $current_year = date("Y");
              $future_year = $current_year + 2;
              $cruise_years = array();
              while ($future_year >= $current_year) {
                $cruise_years[] = $current_year;
                $current_year++;
              }
              foreach ($cruise_years as $cruise_year) {
                echo '<option value="' . $cruise_year . '">' . $cruise_year . '</option>';
              };
            ?>
          </select>
          <input name="submit" type="submit" value="Search" />
        </form>
      </div>
      
    </div>
