<div class="vc_col-sm-3 wpb_column column_container">
  <div class="cruise-card">
      <?php if(get_field('tm_header_image')) : ?>
        <div class="cruise-header-image-container">
          <a href="<?php echo get_permalink() ?>" class="cruise-header-image" style="background-image:url(
            <?php echo wp_get_attachment_image_src(get_field( 'tm_header_image' ), 'medium')[0] ?> )">
          </a>
        </div>
      <?php endif; ?>
      <div class="cruise-card-content">
        <h2><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h2>
        <?php if (strlen(get_field('roundtrip')[0]) > 0) : ?>
          <?php if(get_field('departure_city')) : ?>
            <p><?php echo 'Roundtrip ' . get_field('departure_city'); ?></p>
          <?php endif; ?>
        <?php else : ?>
          <?php if(get_field('departure_city') && get_field('destination_city')) : ?>
            <p><?php echo get_field('departure_city') . ' to ' . get_field('destination_city'); ?></p>
          <?php endif; ?>
        <?php endif; ?>
        <ul class="cruise-details">
          <?php if(get_field('tm_length_days')) : ?>
            <li style="background-image:url(/wp-content/uploads/clock.svg)">
              <span>
                <?php echo (string)(get_field('tm_length_days') - 1) . ' Nights'; ?>
              </span>
            </li>
          <?php endif; ?>
          <!-- Dates Filter - php-partials/archive-dates-filter.php -->
          <?php get_template_part('php-partials/archive','dates-filter'); ?>
          <?php if(get_field('double_occupancy')) : ?>
            <li style="background-image:url(/wp-content/uploads/couple.svg)">
              <span>
                <?php echo 'From $' . number_format(get_field('double_occupancy')); ?>
              </span>
            </li>
          <?php endif; ?>
          <?php if (get_field('cruise_ship')) : ?>
            <?php foreach( get_field('cruise_ship') as $s ): // variable must NOT be called $post (IMPORTANT) ?>
              <li style="background-image:url(/wp-content/uploads/ship.svg)">
                <span>
                  <a href="<?php echo get_permalink( $s->ID ); ?>"><?php echo get_the_title( $s->ID ); ?></a>
                </span>
              </li>
            <?php endforeach; ?>
          <?php endif; ?>
          <?php
            // Load sale data if available
            if (get_field('sale_text') && get_field('sale_end_date')) : ?>
            <?php
              // SALE INFORMATION VARIABLES
              $todays_date = date('m/d/y'); // get date in ACF output format
              $sale_end_date = get_field( 'sale_end_date' ); // sale end date
              $today = strtotime($todays_date); // convert date string to time
              $sale_end = strtotime($sale_end_date); // convert sale string to time
            ?>
            <?php if ($sale_end >= $today) : ?>
              <li style="background-image:url(/wp-content/uploads/star.svg)">
                <span class="sale-text">
                  <?php echo get_field('sale_text'); ?>
                </span>
              </li>
            <?php endif; ?>
          <?php endif; ?>
        </ul>
      </div>
      <?php 
        //Book now param
        $content_id = "cruise_id=".get_the_id();

        //Conditional link depending on trip type
        if (get_post_type(get_the_id()) == 'cruise') : ?>
          <a class="cruise-select book-now" href="/book-now?<?php echo $content_id ?>">Book Now</a>
        <?php else : ?>
          <a class="cruise-select book-now" href="/request-information?referer=<?php echo esc_html( get_the_title() ); ?>">Book Now</a>

        <?php endif; 
      ?>

      <a class="cruise-select" href="<?php echo get_permalink() ?>">More Info</a>
  </div>
</div>
