
<?php
  // VARIABLES FOR CRUISE DATES
  $current_year = date('Y'); // get current year
  $current_year_time = strtotime($current_year); // convert current year to time
  $next_year = date('Y', strtotime('+1 year')); // get next year
  $next_year_time = strtotime($next_year); // convert next year to time
  $todays_date = date('F j, Y'); // get date in ACF output format
  $todays_date_time = strtotime($todays_date); // convert date string to time

?>


<section id="cruise-archive">
  <div class="cruise-archive-content grid-container">
    <?php if (isset($usarc_search_query)) :
      if (! empty($usarc_search_query->posts ) ) :
        foreach ($usarc_search_query->posts as $post ) : setup_postdata($post) ;
          include(get_stylesheet_directory() . '/php-partials/usarc_single_card.php');
        endforeach;
      else :
        echo "<p>Sorry, no trips match your query.</p>";
      endif; ?>
      <div id="pagination" class="clearfix">
      	<?php
          // Custom query loop pagination
          previous_posts_link( '<-- Previous Page |' );
          next_posts_link( '| Next Page -->', $usarc_search_query->max_num_pages );
          // Reset postdata
          wp_reset_postdata();
        ?>
  		</div>
    <?php else:
      if ( have_posts() ) : while ( have_posts() ) : the_post();
        include(get_stylesheet_directory() . '/php-partials/usarc_single_card.php');
      endwhile; else :
         esc_html_e( 'There are currently no cruises for this destination.' );
      endif; ?>
      <div id="pagination" class="clearfix">
        <?php
          posts_nav_link();
          // Reset postdata
          wp_reset_postdata();
        ?>
  		</div>
    <?php endif;
    ?>

  </div>

</section>
