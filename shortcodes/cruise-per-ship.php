<?php

function display_cruises_per_ship(){
    $cruises = get_posts(array(
        'post_type'    => 'cruise',
        'meta_query'	 => array(
            array(
                'key'      => 'cruise_ship',
                'value'    => '"' . get_the_id() . '"',
                'compare'  => 'LIKE'
            )
        ),
    ));

    if ($cruises): ?>
        <ul>
            <?php foreach ($cruises as $cruise) : ?>
                <li>
                    <a href="<?php echo get_permalink($cruise->ID); ?>"><?php echo get_the_title($cruise->ID); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif;

};

add_shortcode( 'ship_cruises', 'display_cruises_per_ship' );

?>