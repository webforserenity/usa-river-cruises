<?php

//Shortcode for search block used on home page
function search_block_func(){

	wp_enqueue_script('search-block');

	ob_start();
    ?>
    <form id="search-icons" method="get" action="/trip-search-results/">
		<div class="input-container">
			<input name="search-radio" type="radio" id="destination-input" checked="checked" value="destination"/>
			<label for="destination-input" class="icon-container">
				<!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In  -->
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#1A2357;}
				</style>
				<defs>
				</defs>
				<path id="Desktop_1_" class="st0" d="M29.2,29.8c-3-0.8-7.2-1.2-11.7-1.2S8.8,29,5.8,29.8C5,30,4.2,30.2,3.7,30.5
					c0.6,0.2,1.3,0.5,2.2,0.7c3,0.8,7.2,1.2,11.7,1.2s8.7-0.5,11.7-1.2c0.9-0.2,1.6-0.5,2.2-0.7C30.8,30.2,30,30,29.2,29.8z M17.5,35
					C7.8,35,0,33,0,30.5s7.8-4.5,17.5-4.5S35,28,35,30.5S27.2,35,17.5,35z M26.9,9.1c0,1.8-1.2,5.4-3.3,10.4c-0.3,0.7-0.6,1.3-0.9,2
					c-0.9,2.1-1.8,4.2-2.8,6.2c-0.3,0.7-0.6,1.4-0.9,2c-0.2,0.3-0.3,0.6-0.4,0.7l-1.2,2.5l-1.2-2.5c-0.1-0.1-0.2-0.4-0.4-0.7
					c-0.3-0.6-0.6-1.2-0.9-2c-1-2-1.9-4.2-2.8-6.2c-0.3-0.7-0.6-1.3-0.8-2c-2.1-5.1-3.3-8.6-3.3-10.4c0-5,4.2-9.1,9.4-9.1
					S26.9,4.1,26.9,9.1z M17.5,26.6c0.9-2,1.9-4.1,2.8-6.1c0.3-0.7,0.6-1.3,0.8-2c1.9-4.7,3.1-8.1,3.1-9.4c0-3.6-3-6.5-6.7-6.5
					s-6.7,2.9-6.7,6.5c0,1.3,1.2,4.8,3.1,9.4c0.3,0.6,0.5,1.3,0.8,1.9C15.6,22.5,16.5,24.6,17.5,26.6C17.5,26.6,17.5,26.7,17.5,26.6
					C17.5,26.7,17.5,26.6,17.5,26.6z M18.8,9.1c0-0.7-0.6-1.3-1.3-1.3s-1.3,0.6-1.3,1.3c0,0.7,0.6,1.3,1.3,1.3S18.8,9.8,18.8,9.1z
					 M21.5,9.1c0,2.1-1.8,3.9-4,3.9s-4-1.7-4-3.9s1.8-3.9,4-3.9S21.5,6.9,21.5,9.1z"/>
				</svg>
	 
          <span class="icon-label">By Destination</span>
      </label>
		</div>

	    <div class="input-container">
			<input name="search-radio" type="radio" id="ship-input" value="ships" />
			<label class="icon-container" for="ship-input">
				<!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In  -->
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px" width="35px" height="35px" viewBox="0 0 35 35" style="enable-background:new 0 0 35 35;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:#1A2357;}
				</style>
				<defs>
				</defs>
				<path id="Desktop_1_" class="st0" d="M17.1,14.6l-16,4.8c-0.5,0.2-0.9,0.6-1,1.2c0,0.1,0,0.2,0,0.3c-0.1,1.2,0.1,2.9,0.8,4.9
					c1,2.9,3,5.9,6.1,8.9C7.2,34.9,7.6,35,8,35H27c0.4,0,0.7-0.1,1-0.4c3.1-3,5.1-6,6.1-8.9c0.7-2,0.9-3.6,0.8-4.9c0-0.1,0-0.2,0-0.3
					c-0.1-0.5-0.5-1-1-1.2l-16-4.8C17.6,14.6,17.4,14.6,17.1,14.6L17.1,14.6z M17.5,17.6l14.6,4.3c0,0.8-0.2,1.7-0.6,2.8
					c-0.8,2.3-2.4,4.8-5,7.4H8.6c-2.6-2.6-4.2-5-5-7.4c-0.4-1.1-0.6-2.1-0.6-2.8L17.5,17.6z M26.2,24.5c0.8,0.2,1.6-0.2,1.8-1
					c0.2-0.8-0.2-1.6-1-1.8L17.9,19c-0.3-0.1-0.6-0.1-0.8,0L8,21.7c-0.8,0.2-1.2,1-1,1.8c0.2,0.8,1,1.2,1.8,1l8.7-2.6L26.2,24.5z
					 M16,8.8V2.9H19v5.8H16z M20.4,11.7c0.8,0,1.5-0.7,1.5-1.5V2.9C21.9,1.3,20.6,0,19,0H16c-1.6,0-2.9,1.3-2.9,2.9v7.3
					c0,0.8,0.7,1.5,1.5,1.5H20.4z M27.7,11.7v7.9c0,0.8,0.7,1.5,1.5,1.5c0.8,0,1.5-0.7,1.5-1.5v-9.3c0-0.8-0.7-1.5-1.5-1.5H5.8
					c-0.8,0-1.5,0.7-1.5,1.5v9.3C4.4,20.3,5,21,5.8,21s1.5-0.7,1.5-1.5v-7.9L27.7,11.7z M14.9,32.1l0.9-4.4h3.4l0.9,4.4H14.9z M21.9,35
					c0.9,0,1.6-0.8,1.4-1.7L21.8,26c-0.1-0.7-0.7-1.2-1.4-1.2h-5.8c-0.7,0-1.3,0.5-1.4,1.2l-1.5,7.3c-0.2,0.9,0.5,1.7,1.4,1.7H21.9z
					 M1.5,35h32.1c0.8,0,1.5-0.7,1.5-1.5s-0.7-1.5-1.5-1.5H1.5c-0.8,0-1.5,0.7-1.5,1.5S0.7,35,1.5,35z"/>
				</svg>
	 
	 
          <span class="icon-label">By Ship</span>
      </label>
		</div>
		<div class="input-container">
			<input name="search-radio" type="radio" id="rail-input" value="rail" />
			<label class="icon-container" for="rail-input">
				<!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In  -->
				<svg version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px" width="32px" height="35.6px" viewBox="0 0 32 35.6" style="enable-background:new 0 0 32 35.6;"
					 xml:space="preserve">
				<style type="text/css">
					.st0{fill:#1C2555;}
				</style>
				<defs>
				</defs>
				<path class="st0" d="M8.5,20.6c-1.3,0-2.4,1.1-2.4,2.4s1.1,2.4,2.4,2.4c1.3,0,2.4-1.1,2.4-2.4C10.9,21.7,9.8,20.6,8.5,20.6z
					 M8.5,23.8c-0.4,0-0.8-0.4-0.8-0.8c0-0.4,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8C9.3,23.4,8.9,23.8,8.5,23.8z M23.5,20.6
					c-1.3,0-2.4,1.1-2.4,2.4c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1.1,2.4-2.4c0,0,0,0,0,0C25.9,21.7,24.8,20.6,23.5,20.6z M31.8,34.5L27,29
					c1.3-0.5,2.2-1.8,2.2-3.2V11.8c0-5-4.1-9-9-9h-0.7V2.1c0-1.2-0.9-2.1-2.1-2.1h-2.8c-1.2,0-2.1,0.9-2.1,2.1v0.7h-0.7
					c-2.5,0-4.9,1.1-6.7,2.9C5.1,5.8,5,5.8,5,5.9c-1.4,1.6-2.2,3.7-2.2,5.9v13.9c0,1.4,0.9,2.7,2.2,3.2l-4.8,5.5c-0.3,0.3-0.2,0.7,0.1,1
					c0.1,0.1,0.3,0.2,0.5,0.2h4.2c0.2,0,0.4-0.1,0.6-0.3l4.7-6.1h11.8l4.7,6.1c0.1,0.2,0.3,0.3,0.6,0.3h4.2c0.4,0,0.7-0.3,0.7-0.7
					C32,34.8,31.9,34.6,31.8,34.5z M27.3,11.4v4.5H17.1V7h8.6C26.8,8.2,27.4,9.8,27.3,11.4z M13.9,2.2c0-0.3,0.3-0.5,0.7-0.5h2.8
					c0.4,0,0.7,0.2,0.7,0.5v0.5h-4.2V2.2z M11.8,4.4h8.3c1.6,0,3.1,0.4,4.4,1.1H7.4C8.7,4.8,10.3,4.4,11.8,4.4z M4.7,11.4
					c0-1.6,0.6-3.2,1.6-4.4h8.5v8.9H4.7V11.4z M4.5,34.1H2.4l4-4.6H8L4.5,34.1z M6.4,27.5c-1.1,0-2-0.9-2-2v-7.4h23v7.4c0,1.1-0.9,2-2,2
					H6.4z M27.5,34.2l-3.6-4.7h1.7l4.1,4.7H27.5z M18.5,20.5h-5c-0.4,0-0.7,0.4-0.7,0.8c0,0.4,0.3,0.8,0.7,0.8h5c0.4,0,0.7-0.4,0.7-0.8
					C19.2,20.9,18.9,20.5,18.5,20.5z M13.5,25.4h5c0.4,0,0.7-0.4,0.7-0.8c0-0.4-0.3-0.8-0.7-0.8h-5c-0.4,0-0.7,0.4-0.7,0.8
					C12.8,25,13.1,25.4,13.5,25.4z M23.5,23.8c-0.4,0-0.8-0.4-0.8-0.8c0-0.4,0.4-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8
					C24.3,23.4,24,23.8,23.5,23.8z"/>
				</svg>
 
          <span class="icon-label">By Rail</span>
      </label>
		</div>
		<div class="dropdown-container">
			<select name="destination" id="destinations-select">
				<option disabled selected value="">Destination</option>
				<?php
          $_terms = get_terms( array('destination') );
          foreach ($_terms as $term) :
            echo '<option value="' . $term->slug .'">' . $term->name . '</option>';
          endforeach ;
        ?>
			</select>
			<select name="ships" id="ships-select" style="display:none">
				<option disabled selected value=
				"">Ship</option>
				<?php
          $_ships = get_posts(array(
						'orderby'	         => 'title',
						'order'            => 'ASC',
						'posts_per_page'	 => '-1',
						'post_type'	       => 'ship',
					));
					foreach ($_ships as $s) :
						echo '<option value="' . $s->ID .'">' . $s->post_title . '</option>';
					endforeach;
        ?>

			</select>
			<select name="rail" id="rails-select" style="display:none">
				<option disabled selected value="">Rail Destination</option>
				<?php
          $_terms = get_terms( array('destination') );
          foreach ($_terms as $term) :
            echo '<option value="' . $term->slug .'">' . $term->name . '</option>';
          endforeach ;
        ?>
			</select>
			<input name="monthyear" placeholder="Departure" type="text" id="monthpicker" class="month-year-input" />
			<br/>
			<input name="submit" type="submit" value="Search" />

		</div>
    </form>

    <?php return ob_get_clean();
}

add_shortcode( 'search_block', 'search_block_func' );

?>
