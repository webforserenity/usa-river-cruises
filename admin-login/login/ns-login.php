<?php

include (dirname(__FILE__) . '/../ns-config.php');

/*
*
* This file contains functions for the following:
* - Login Screen
*
*/


/*
* Loads Stylesheet For Login Page
*/

function ns_login() {
	echo '<link rel="stylesheet" type="text/css" href="' . get_stylesheet_directory_uri() . '/admin-login/login/styles.css" />';
}

add_action('login_head', 'ns_login');

/*
* Updates the URL for the new logo.
*/

function ns_login_logo_url() {
  return home_url();
}

add_filter( 'login_headerurl', 'ns_login_logo_url' );

/*
* Updates the title tag for the new logo.
*/

function ns_login_logo_url_title() {
	global $login_logo_title;
  return $login_logo_title;
}

add_filter( 'login_headertitle', 'ns_login_logo_url_title' );