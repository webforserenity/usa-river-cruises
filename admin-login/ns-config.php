<?php

/*
* Accepted Variables for Confirmation
*/
$confirmed = array('Yes','yes','Y','y');

/*
* WORDPRESS ADMIN STYLES
*/

$client_name = "USA River Cruises";

/*
* Changes Howdy Text
*
* Welcome, User Name or User Name
*/
$change_howdy_text = "yes";
$howdy_text = "Welcome";
/*
* Load Admin CSS
*
* Would you like to load Admin CSS styles?
*/
$ns_admin_css = "yes";
/*
* Changes WordPress admin footer text.
*
* New text: Client Name, Wordpress Version
*/
$custom_admin_footer = "yes";
/*
* Remove Various Meta Boxes by title
*/
$rm_wordpress_news = "yes"; // WordPress News
$rm_quick_draft = "yes"; // Quick Draft
$rm_at_a_glance = "no"; // At a Glance
$rm_activity = "no"; // Activity
$rm_wp_edit = "yes"; // WP Edit Pro RSS Feed

$login_logo_title = $client_name;