<?php


include (dirname(__FILE__) . '/../ns-config.php');


/*
* Loads Admin CSS
*/
global $ns_admin_css, $confirmed;
if ( in_array($ns_admin_css, $confirmed) ){
	function ns_admin_styles() {
		wp_enqueue_style(
	    	'ns-admin-css',
	    	get_stylesheet_directory_uri() . '/admin-login/admin/styles.css' );
	}
	add_action('admin_enqueue_scripts', 'ns_admin_styles');
}

/*
* Changes the "Howdy" text in the admin bar
*/

global $change_howdy_text,$confirmed;
if ( in_array($change_howdy_text, $confirmed) ){
  function ns_change_howdy($translated, $text, $domain) {
  	global $howdy_text;
		if (!is_admin() || 'default' != $domain)
    	return str_replace('Howdy', $howdy_text, $translated);

  	if (false !== strpos($translated, 'Howdy'))
    return str_replace('Howdy', $howdy_text, $translated);

  	return $translated;
	}
	add_filter('gettext', 'ns_change_howdy', 10, 3);
}



/*
* Removes default dashboard widgets
*/

function remove_dashboard_meta() {
	global $confirmed, $rm_wordpress_news, $rm_quick_draft, $rm_at_a_glance, $rm_jupiter_post_stats, $rm_activity, $rm_wp_edit;
	if ( in_array($rm_wordpress_news, $confirmed) ){
		remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' ); // WordPress News
	}
	if ( in_array($rm_quick_draft, $confirmed) ){
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' ); // Quick Draft
	}
	if ( in_array($rm_at_a_glance, $confirmed) ){
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' ); // At a Glance
	}
	if ( in_array($rm_jupiter_post_stats, $confirmed) ){
		remove_meta_box( 'mk_posts_like_stats', 'dashboard', 'normal' ); // Jupiter Post Stats
	}
	if ( in_array($rm_activity, $confirmed) ){
		remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' ); // Activity
	}
	if ( in_array($rm_wp_edit, $confirmed) ){
		remove_meta_box( 'jwl_user_tinymce_dashboard_widget', 'dashboard', 'normal' ); // WP Edit Pro RSS Feed
	}
}

add_action( 'admin_init', 'remove_dashboard_meta' );

/*
* Creates Custom footer in backend
*/

function ns_custom_admin_footer() {
	global $confirmed, $client_name, $custom_admin_footer;
	if( in_array($custom_admin_footer, $confirmed)){
		echo sprintf( __( '%s — WordPress Version: %s'),$client_name, get_bloginfo ( 'version' ) );
	}
	else{
		echo sprintf( __( 'Thank you for creating with <a href="%s">WordPress</a>.' ), __( 'https://wordpress.org/' ) );
	}
}

add_filter( 'admin_footer_text', 'ns_custom_admin_footer' );