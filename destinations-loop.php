<?php
/**
 * Template Name: Destinations
 *
 * @package WordPress
 * @subpackage Jupter
 * @since USA River Cruises 1.0
 */

get_header();

?>
<div id="destinations-loop">
  <div class="destinations-container">
    <?php
      $_terms = get_terms( array('destination') );
      foreach ($_terms as $term) :
        $term_slug = $term->slug;
        $_posts = new WP_Query( array(
                    'post_type'         => array('cruise','train'),
                    'posts_per_page'    => 10, //important for a PHP memory limit warning
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'destination',
                            'field'    => 'slug',
                            'terms'    => $term_slug,
                        ),
                    ),
                ));
        if( $_posts->have_posts() ) :
          $term_link = get_term_link( $term );
          $term_id = $term->term_id;
          $image_id = get_term_meta($term_id, 'destination-image-id', true);
          //Get image object at medium size
          $image_thumb = wp_get_attachment_image_src( $image_id, 'medium', false, ''); ?>
          <div class="vc_col-sm-3 wpb_column column_container">
            <div class="destination-card" style="background-image:url(<?php echo $image_thumb[0] ?>)">
              <a href="<?php echo $term_link ?>"><?php echo $term->name ?></a>
            </div>
          </div>
        <?php endif;
        wp_reset_postdata();
      endforeach;
    ?>
  </div>

</div>

<?php

get_footer();

?>
