<?php

if ( ! function_exists('train_post_type') ) {

// Register Custom Post Type
function train_post_type() {

	$labels = array(
		'name'                  => _x( 'Trains', 'Post Type General Name', 'jupiter' ),
		'singular_name'         => _x( 'Train', 'Post Type Singular Name', 'jupiter' ),
		'menu_name'             => __( 'Trains', 'jupiter' ),
		'name_admin_bar'        => __( 'Trains', 'jupiter' ),
		'archives'              => __( 'Train Archives', 'jupiter' ),
		'attributes'            => __( 'Train Attributes', 'jupiter' ),
		'parent_item_colon'     => __( 'Train', 'jupiter' ),
		'all_items'             => __( 'All Trains', 'jupiter' ),
		'add_new_item'          => __( 'Add New Train', 'jupiter' ),
		'add_new'               => __( 'Add New Train', 'jupiter' ),
		'new_item'              => __( 'New Train', 'jupiter' ),
		'edit_item'             => __( 'Edit Train', 'jupiter' ),
		'update_item'           => __( 'Update Train', 'jupiter' ),
		'view_item'             => __( 'View Train', 'jupiter' ),
		'view_items'            => __( 'View Trains', 'jupiter' ),
		'search_items'          => __( 'Search Trains', 'jupiter' ),
		'not_found'             => __( 'No Trains found', 'jupiter' ),
		'not_found_in_trash'    => __( 'No Trains found in Trash', 'jupiter' ),
		'featured_image'        => __( 'Featured Image', 'jupiter' ),
		'set_featured_image'    => __( 'Set featured image', 'jupiter' ),
		'remove_featured_image' => __( 'Remove featured image', 'jupiter' ),
		'use_featured_image'    => __( 'Use as featured image', 'jupiter' ),
		'insert_into_item'      => __( 'Insert into item', 'jupiter' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'jupiter' ),
		'items_list'            => __( 'Items list', 'jupiter' ),
		'items_list_navigation' => __( 'Items list navigation', 'jupiter' ),
		'filter_items_list'     => __( 'Filter items list', 'jupiter' ),
	);
	$rewrite = array(
		'slug'                  => 'train',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'train', 'jupiter' ),
		'description'           => __( 'Train Post Type', 'jupiter' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author','revisions', 'custom-fields', 'page-attributes', ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-usarc-train',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'train',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'train', $args );

}
add_action( 'init', 'train_post_type', 0 );


// Flush rewrite rules on theme switch.

function train_rewrite_flush() {
    train_post_type();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'train_rewrite_flush' );

}
