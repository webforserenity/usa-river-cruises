<?php
    /**
     * Initialize the class and start calling our hooks and filters
     */

     // Image actions
     add_action( 'destination_add_form_fields', 'add_destination_image', 10, 2 );
     add_action( 'created_destination', 'save_destination_image',  10, 2 );
     add_action( 'destination_edit_form_fields', 'update_destination_image', 10, 2 );
     add_action( 'edited_destination', 'updated_destination_image', 10, 2 );
     add_action( 'admin_enqueue_scripts', 'load_media' );
     add_action( 'admin_footer', 'add_script');

   function load_media() {
     if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'destination' ) {
       return;
     }
     wp_enqueue_media();
   }

   function add_destination_image( $taxonomy ) { ?>
     <div class="form-field term-group">
       <label for="destination-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
       <input type="hidden" id="destination-image-id" name="destination-image-id" class="custom_media_url" value="">
       <div id="destination-image-wrapper"></div>
       <p>
         <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
         <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
       </p>
     </div>
   <?php }

   function save_destination_image( $term_id, $tt_id ) {
     if( isset( $_POST['destination-image-id'] ) && '' !== $_POST['destination-image-id'] ){
       add_term_meta( $term_id, 'destination-image-id', absint( $_POST['destination-image-id'] ), true );
     }
    }
    function update_destination_image( $term, $taxonomy ) { ?>
      <tr class="form-field term-group-wrap">
        <th scope="row">
          <label for="destination-image-id"><?php _e( 'Image', 'showcase' ); ?></label>
        </th>
        <td>
          <?php $image_id = get_term_meta( $term->term_id, 'destination-image-id', true ); ?>
          <input type="hidden" id="destination-image-id" name="destination-image-id" value="<?php echo esc_attr( $image_id ); ?>">
          <div id="destination-image-wrapper">
            <?php if( $image_id ) { ?>
              <?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?>
            <?php } ?>
          </div>
          <p>
            <input type="button" class="button button-secondary showcase_tax_media_button" id="showcase_tax_media_button" name="showcase_tax_media_button" value="<?php _e( 'Add Image', 'showcase' ); ?>" />
            <input type="button" class="button button-secondary showcase_tax_media_remove" id="showcase_tax_media_remove" name="showcase_tax_media_remove" value="<?php _e( 'Remove Image', 'showcase' ); ?>" />
          </p>
        </td>
      </tr>
   <?php }

   /**
    * Update the form field value
    * @since 1.0.0
    */
   function updated_destination_image( $term_id, $tt_id ) {
     if( isset( $_POST['destination-image-id'] ) && '' !== $_POST['destination-image-id'] ){
       update_term_meta( $term_id, 'destination-image-id', absint( $_POST['destination-image-id'] ) );
     } else {
       update_term_meta( $term_id, 'destination-image-id', '' );
     }
   }

   function add_script() {
     if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'destination' ) {
       return;
     } ?>
     <script> jQuery(document).ready( function($) {
       _wpMediaViewsL10n.insertIntoPost = '<?php _e( "Insert", "showcase" ); ?>';
       function ct_media_upload(button_class) {
         var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
         $('body').on('click', button_class, function(e) {
           var button_id = '#'+$(this).attr('id');
           var send_attachment_bkp = wp.media.editor.send.attachment;
           var button = $(button_id);
           _custom_media = true;
           wp.media.editor.send.attachment = function(props, attachment){
             if( _custom_media ) {
               $('#destination-image-id').val(attachment.id);
               $('#destination-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
               $( '#destination-image-wrapper .custom_media_image' ).attr( 'src',attachment.url ).css( 'display','block' );
             } else {
               return _orig_send_attachment.apply( button_id, [props, attachment] );
             }
           }
           wp.media.editor.open(button); return false;
         });
       }
       ct_media_upload('.showcase_tax_media_button.button');
       $('body').on('click','.showcase_tax_media_remove',function(){
         $('#destination-image-id').val('');
         $('#destination-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
       });
       // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-destination-ajax-response
       $(document).ajaxComplete(function(event, xhr, settings) {
         var queryStringArr = settings.data.split('&');
         if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
           var xml = xhr.responseXML;
           $response = $(xml).find('term_id').text();
           if($response!=""){
             // Clear the thumb image
             $('#destination-image-wrapper').html('');
           }
          }
        });
      });
    </script>
   <?php }
?>
