<?php

if ( ! function_exists('cruise_post_type') ) {

// Register Custom Post Type
function cruise_post_type() {

	$labels = array(
		'name'                  => _x( 'Cruises', 'Post Type General Name', 'jupiter' ),
		'singular_name'         => _x( 'Cruise', 'Post Type Singular Name', 'jupiter' ),
		'menu_name'             => __( 'Cruises', 'jupiter' ),
		'name_admin_bar'        => __( 'Cruises', 'jupiter' ),
		'archives'              => __( 'Cruise Archives', 'jupiter' ),
		'attributes'            => __( 'Cruise Attributes', 'jupiter' ),
		'parent_item_colon'     => __( 'Cruise', 'jupiter' ),
		'all_items'             => __( 'All Cruises', 'jupiter' ),
		'add_new_item'          => __( 'Add New Cruise', 'jupiter' ),
		'add_new'               => __( 'Add New Cruise', 'jupiter' ),
		'new_item'              => __( 'New Cruise', 'jupiter' ),
		'edit_item'             => __( 'Edit Cruise', 'jupiter' ),
		'update_item'           => __( 'Update Cruise', 'jupiter' ),
		'view_item'             => __( 'View Cruise', 'jupiter' ),
		'view_items'            => __( 'View Cruises', 'jupiter' ),
		'search_items'          => __( 'Search Cruises', 'jupiter' ),
		'not_found'             => __( 'No Cruises found', 'jupiter' ),
		'not_found_in_trash'    => __( 'No Cruises found in Trash', 'jupiter' ),
		'featured_image'        => __( 'Featured Image', 'jupiter' ),
		'set_featured_image'    => __( 'Set featured image', 'jupiter' ),
		'remove_featured_image' => __( 'Remove featured image', 'jupiter' ),
		'use_featured_image'    => __( 'Use as featured image', 'jupiter' ),
		'insert_into_item'      => __( 'Insert into item', 'jupiter' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'jupiter' ),
		'items_list'            => __( 'Items list', 'jupiter' ),
		'items_list_navigation' => __( 'Items list navigation', 'jupiter' ),
		'filter_items_list'     => __( 'Filter items list', 'jupiter' ),
	);
	$rewrite = array(
		'slug'                  => 'cruise',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Cruise', 'jupiter' ),
		'description'           => __( 'Cruise Post Type', 'jupiter' ),
		'labels'                => $labels,
		'supports'              => array('editor', 'title','author','revisions', 'custom-fields'),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-usarc-cruise',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'cruise', $args );

}
add_action( 'init', 'cruise_post_type', 0 );

// Flush rewrite rules on theme switch.

function cruise_rewrite_flush() {
    cruise_post_type();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'cruise_rewrite_flush' );

}
