<?php

// Register Destinations taxonomy for cruises and ships
function usarc_destinations_taxonomy() {
	$labels = array(
		'name'                         => _x('Destinations', 'taxonomy general name', 'jupiter'),
		'singular_name'	               => _x('Destination', 'taxonomy singular name', 'jupiter'),
		'search_items'	               => __('Search Destinations', 'jupiter'),
		'popular_items'	               => null,
		'all_items'	                   => __('All Destinations', 'jupiter'),
		'parent_item'	                 => null,
		'parent_item_colon'	           => null,
		'edit_item'	                   => __('Edit Destination', 'jupiter'),
		'update_item'	                 => __('Update Destination', 'jupiter'),
		'add_new_item'                 => __( 'Add New Destination', 'jupiter' ),
		'new_item_name'                => __( 'New Destination Name', 'jupiter'),
		'separate_items_with_commas' 	 => __('Separate Destinations with commas', 'jupiter'),
		'add_or_remove_items'          => __('Add or remove destinations', 'jupiter'),
		'choose_from_most_used'        => null,
		'not_found'                    => __('No Destinations found', 'jupiter'),
	);
	$rewrite = array(
		'slug'                       => '',
		'with_front'                 => false,
		'hierarchical'               => true,
	);

	$args = array(
		'labels'	             => $labels,
		'hierarchical'			   => true,
		'show_in_rest'	       => true,
		'show_tag_cloud'	     => false,
		'show_admin_column'	   => true,
		'show_in_nav_menus'		 => true,
		'rewrite'				       => $rewrite
	);

	//Allow shortcodes to be used in descriptions.
	add_filter( 'term_description', 'do_shortcode' );

	register_taxonomy('destination', array('cruise','train'), $args);
};

add_action('init', 'usarc_destinations_taxonomy');

// Create Image Upload for Destination Taxonomy
include('destination_image_upload/destination_image_upload.php');

if ( ! function_exists( 'cruise_line' ) ) {

// Register Custom Taxonomy
function cruise_line() {

	$labels = array(
		'name'                       => _x( 'Cruise Lines', 'Taxonomy General Name', 'jupiter' ),
		'singular_name'              => _x( 'Cruise Line', 'Taxonomy Singular Name', 'jupiter' ),
		'menu_name'                  => __( 'Cruise Line', 'jupiter' ),
		'all_items'                  => __( 'All Cruise Lines', 'jupiter' ),
		'parent_item'                => __( 'Parent Cruise Line', 'jupiter' ),
		'parent_item_colon'          => __( 'Parent Cruise Line:', 'jupiter' ),
		'new_item_name'              => __( 'New Cruise Line', 'jupiter' ),
		'add_new_item'               => __( 'Add New Cruise Line', 'jupiter' ),
		'edit_item'                  => __( 'Edit Cruise Line', 'jupiter' ),
		'update_item'                => __( 'Update Cruise Line', 'jupiter' ),
		'view_item'                  => __( 'View Cruise Line', 'jupiter' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'jupiter' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'jupiter' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'jupiter' ),
		'popular_items'              => __( 'Popular Items', 'jupiter' ),
		'search_items'               => __( 'Search Items', 'jupiter' ),
		'not_found'                  => __( 'Not Found', 'jupiter' ),
		'no_terms'                   => __( 'No items', 'jupiter' ),
		'items_list'                 => __( 'Items list', 'jupiter' ),
		'items_list_navigation'      => __( 'Items list navigation', 'jupiter' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'cruise_line', array( 'cruise' ), $args );

}
add_action( 'init', 'cruise_line', 0 );

}