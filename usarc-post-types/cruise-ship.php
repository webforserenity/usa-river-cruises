<?php

if ( ! function_exists('ship_post_type') ) {

// Register Custom Post Type
function ship_post_type() {

	$labels = array(
		'name'                  => _x( 'Ships', 'Post Type General Name', 'jupiter' ),
		'singular_name'         => _x( 'Ship', 'Post Type Singular Name', 'jupiter' ),
		'menu_name'             => __( 'Ships', 'jupiter' ),
		'name_admin_bar'        => __( 'Ships', 'jupiter' ),
		'archives'              => __( 'Ship Archives', 'jupiter' ),
		'attributes'            => __( 'Ship Attributes', 'jupiter' ),
		'parent_item_colon'     => __( 'Ship', 'jupiter' ),
		'all_items'             => __( 'All Ships', 'jupiter' ),
		'add_new_item'          => __( 'Add New Ship', 'jupiter' ),
		'add_new'               => __( 'Add New Ship', 'jupiter' ),
		'new_item'              => __( 'New Ship', 'jupiter' ),
		'edit_item'             => __( 'Edit Ship', 'jupiter' ),
		'update_item'           => __( 'Update Ship', 'jupiter' ),
		'view_item'             => __( 'View Ship', 'jupiter' ),
		'view_items'            => __( 'View Ships', 'jupiter' ),
		'search_items'          => __( 'Search Ships', 'jupiter' ),
		'not_found'             => __( 'No Ships found', 'jupiter' ),
		'not_found_in_trash'    => __( 'No Ships found in Trash', 'jupiter' ),
		'featured_image'        => __( 'Featured Image', 'jupiter' ),
		'set_featured_image'    => __( 'Set featured image', 'jupiter' ),
		'remove_featured_image' => __( 'Remove featured image', 'jupiter' ),
		'use_featured_image'    => __( 'Use as featured image', 'jupiter' ),
		'insert_into_item'      => __( 'Insert into item', 'jupiter' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'jupiter' ),
		'items_list'            => __( 'Items list', 'jupiter' ),
		'items_list_navigation' => __( 'Items list navigation', 'jupiter' ),
		'filter_items_list'     => __( 'Filter items list', 'jupiter' ),
	);
	$rewrite = array(
		'slug'                  => 'ship',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Ship', 'jupiter' ),
		'description'           => __( 'Ship Post Type', 'jupiter' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-usarc-ship',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'ships',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'ship', $args );

}
add_action( 'init', 'ship_post_type', 0 );


// Flush rewrite rules on theme switch.

function ship_rewrite_flush() {
    ship_post_type();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'ship_rewrite_flush' );

}
