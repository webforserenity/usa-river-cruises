<?php

if ( ! function_exists('combination_post_type') ) {

// Register Custom Post Type
function combination_post_type() {

	$labels = array(
		'name'                  => _x( 'Combo', 'Post Type General Name', 'jupiter' ),
		'singular_name'         => _x( 'Combo', 'Post Type Singular Name', 'jupiter' ),
		'menu_name'             => __( 'Combo', 'jupiter' ),
		'name_admin_bar'        => __( 'Combo', 'jupiter' ),
		'archives'              => __( 'Combo Archives', 'jupiter' ),
		'attributes'            => __( 'Combo Attributes', 'jupiter' ),
		'parent_item_colon'     => __( 'Combo', 'jupiter' ),
		'all_items'             => __( 'All Combos', 'jupiter' ),
		'add_new_item'          => __( 'Add New Combo', 'jupiter' ),
		'add_new'               => __( 'Add New Combo', 'jupiter' ),
		'new_item'              => __( 'New Combo', 'jupiter' ),
		'edit_item'             => __( 'Edit Combo', 'jupiter' ),
		'update_item'           => __( 'Update Combo', 'jupiter' ),
		'view_item'             => __( 'View Combo', 'jupiter' ),
		'view_items'            => __( 'View Combos', 'jupiter' ),
		'search_items'          => __( 'Search Combos', 'jupiter' ),
		'not_found'             => __( 'No Combos found', 'jupiter' ),
		'not_found_in_trash'    => __( 'No Combos found in Trash', 'jupiter' ),
		'featured_image'        => __( 'Featured Image', 'jupiter' ),
		'set_featured_image'    => __( 'Set featured image', 'jupiter' ),
		'remove_featured_image' => __( 'Remove featured image', 'jupiter' ),
		'use_featured_image'    => __( 'Use as featured image', 'jupiter' ),
		'insert_into_item'      => __( 'Insert into item', 'jupiter' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'jupiter' ),
		'items_list'            => __( 'Items list', 'jupiter' ),
		'items_list_navigation' => __( 'Items list navigation', 'jupiter' ),
		'filter_items_list'     => __( 'Filter items list', 'jupiter' ),
	);
	$rewrite = array(
		'slug'                  => 'combination',
		'with_front'            => false,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Combo', 'jupiter' ),
		'description'           => __( 'Combo Post Type', 'jupiter' ),
		'labels'                => $labels,
		'supports'              => array('editor', 'title','author','revisions', 'custom-fields'),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-usarc-combo',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'combination', $args );

}
add_action( 'init', 'combination_post_type', 0 );

// Flush rewrite rules on theme switch.

function combo_rewrite_flush() {
    combination_post_type();
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'combo_rewrite_flush' );

}
