<?php

get_header();

// Get the current taxonomy
$taxonomy = $wp_query->get_queried_object();
$term_id = $taxonomy->term_id;
$image_id = get_term_meta($term_id, 'destination-image-id', true);
$image_src = wp_get_attachment_url($image_id);

//Redirect Destinage loop to full URL slug path
if ($taxonomy->parent > 0 ){ 
  $destination_parents = get_term_parents_list( 
    $taxonomy->term_id, 
    'destination', 
    array( 
      'format' => 'slug', 
      'link'  => 'false', 
    ) 
  ); 
  $requested_url = home_url() . '/destination/' . $wp_query->query['destination'] . '/'; 
  // point page to page with full url, hope it doesn't cause infinite redirect loop 
  if ($requested_url != (home_url() . '/destination/' . $destination_parents)){ 
    wp_safe_redirect(home_url() . '/destination/' . $destination_parents, 301); 
    exit; 
  } 
} 
?>

<section id="destination-taxonomy">
  <div class="archive-header-container" style="background-image:url(<?php echo $image_src ?>)">
    <div class="overlay">
      <div class="cruise-archive-content ">
        <div class="description"><?php echo category_description() ?></div>
      </div>
    </div>
  </div>
</section>

<?php

include(get_stylesheet_directory() . '/php-partials/usarc_cards.php');

get_footer();

?>
