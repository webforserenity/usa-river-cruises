<?php
global $mk_options;

$mk_footer_class = $show_footer = $disable_mobile = $footer_status = '';

$post_id = global_get_post_id();
if ( $post_id ) {
	$show_footer = get_post_meta( $post_id, '_template', true );
	$cases = array( 'no-footer', 'no-header-footer', 'no-header-title-footer', 'no-footer-title' );
	$footer_status = in_array( $show_footer, $cases );
}

if ( $mk_options['disable_footer'] == 'false' || ( $footer_status ) ) {
	$mk_footer_class .= ' mk-footer-disable';
}

if ( $mk_options['footer_type'] == '2' ) {
	$mk_footer_class .= ' mk-footer-unfold';
}


$boxed_footer = (isset( $mk_options['boxed_footer'] ) && ! empty( $mk_options['boxed_footer'] )) ? $mk_options['boxed_footer'] : 'true';
$footer_grid_status = ($boxed_footer == 'true') ? ' mk-grid' : ' fullwidth-footer';
$disable_mobile = ($mk_options['footer_disable_mobile'] == 'true' ) ? $mk_footer_class .= ' disable-on-mobile' : ' ';

?>

<section id="mk-footer-unfold-spacer"></section>

<section id="mk-footer" class="<?php echo $mk_footer_class; ?>" <?php echo get_schema_markup( 'footer' ); ?>>
	<?php if ( $mk_options['disable_footer'] == 'true' && ! $footer_status ) : ?>
	<div class="footer-wrapper<?php echo $footer_grid_status; ?>">
		<div class="mk-padding-wrapper">
			<?php mk_get_view( 'footer', 'widgets' ); ?>
			<div class="clearboth"></div>
		</div>
	</div>
	<?php endif; ?>
	<?php
	if ( $mk_options['disable_sub_footer'] == 'true' && ! $footer_status ) {
		mk_get_view(
			'footer', 'sub-footer', false, [
				'footer_grid_status' => $footer_grid_status,
			]
		);
	}
?>
</section>
</div>
<?php
	global $is_header_shortcode_added;

if ( $mk_options['seondary_header_for_all'] === 'true' || get_header_style() === '3' || $is_header_shortcode_added === '3' ) {
	mk_get_header_view(
		'holders', 'secondary-menu', [
			'header_shortcode_style' => $is_header_shortcode_added,
		]
	);
}
?>
</div>

<div class="bottom-corner-btns js-bottom-corner-btns">
<?php
if ( $mk_options['go_to_top'] != 'false' ) {
	mk_get_view( 'footer', 'navigate-top' );
}

if ( $mk_options['disable_quick_contact'] != 'false' ) {
	mk_get_view( 'footer', 'quick-contact' );
}

		do_action( 'add_to_cart_responsive' );
?>
</div>


<?php
if ( $mk_options['header_search_location'] === 'fullscreen_search' ) {
	mk_get_header_view( 'global', 'full-screen-search' );
}
?>

<?php if ( ! empty( $mk_options['body_border'] ) && $mk_options['body_border'] === 'true' ) { ?>
	<div class="border-body border-body--top"></div>
	<div class="border-body border-body--left border-body--side"></div>
	<div class="border-body border-body--right border-body--side"></div>
	<div class="border-body border-body--bottom"></div>
<?php } ?>

	<?php wp_footer(); ?>
	<!-- Google Code for Remarketing Tag -->
	<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: //google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript" defer>
	/* <![CDATA[ */
	var google_conversion_id = 960051355;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="Google Tracking" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/960051355/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
</body>
</html>
