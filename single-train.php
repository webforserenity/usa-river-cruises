<?php
/*
* PAGE LAYOUT FOR SINGLE TRAIN PACKAGE
*
* Used with the train post type
*
*/

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="master-holder clearfix" id="theme-page">
  <div class="mk-page-section-wrapper">
    <div class="mk-page-section self-hosted full_layout full-width-2 js-el js-master-row mk-in-viewport" id="tm--header">
      <?php get_template_part('php-partials/single-travel-mode/content','header'); ?>
      <div class="clearboth"></div>
    </div>
  </div>
  <?php get_template_part('php-partials/single-travel-mode/content','main'); ?>
</div>
<div id="content--marketing">
<?php the_content(); ?>
</div>
<?php get_template_part('php-partials/single-travel-mode/content','book-now'); ?>
<?php
endwhile;
endif;
get_footer();
