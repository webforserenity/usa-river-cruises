<?php

// Ignore
$confirmed = array('Yes','yes','Y','y');

/*
* FILL OUT THESE OPTIONS
*
* These are options for loading additional functions. Fill out yes or no.
*/
$load_admin_styles = "yes";
$load_login_styles = "yes";
$load_js = "yes";

/*
* CUSTOM POST TYPES
*
* Initialize the custom post types
*/

function include_all_php($folder){
 foreach (glob("{$folder}/*.php") as $filename)
 {
    include $filename;
 }
}
include_all_php(__DIR__ . "/usarc-post-types");

/*
* Admin Settings
*
* Enables options for Admin CSS, footer callout, and Howdy Text.
*/

if ( in_array($load_admin_styles, $confirmed) ){
	// Load Admin Functions
	include_once 'admin-login/admin/ns-admin.php';
}

/*
* Login Settings
*
* Enables options for Login page.
*/

if ( in_array($load_login_styles, $confirmed) ){
	// Load Login Functions
	include_once 'admin-login/login/ns-login.php';
}

//SHORTCODES
include_once(get_stylesheet_directory() . '/shortcodes/search-block.php');
include_once(get_stylesheet_directory() . '/shortcodes/cruise-per-ship.php');

/*
* LOAD JQUERY
*
* Loads any additional jQuery
*/

if ( in_array($load_js, $confirmed) ){
	function ns_scripts_method() {
        //smooth theme jquery-ui
        wp_enqueue_style('jquery-ui-smooth', 'https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css');

        //monthpicker styles
        wp_enqueue_style('monthpicker', get_stylesheet_directory_uri() . '/includes/monthpicker/MonthPicker.css');

        //datepicker and MonthPicker
        //Enqueue date picker UI from WP core:
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('jquery-ui-button');
        //Enqueue the jQuery UI theme css file from google:
        wp_enqueue_style('e2b-admin-ui-css','https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css',false,"1.9.0",false);

        //monthpicker
        wp_enqueue_script(
            'monthpicker',
      		  get_stylesheet_directory_uri() . '/includes/monthpicker/MonthPicker.js',
      		  array( 'jquery' ),
      		  '',
      		  true
        );

        //Webfor JS
        wp_enqueue_script(
    		  'webfor-js',
    		  get_stylesheet_directory_uri() . '/js/min/webfor-min.js',
    		  array( 'jquery' ),
    		  '',
    		  true
        );
        
        //Alert Bar JS
        wp_enqueue_script(
    		  'alert-bar-js',
    		  get_stylesheet_directory_uri() . '/js/min/alertbar-min.js',
    		  array( 'jquery' ),
    		  '',
    		  true
        );
        
        /** Trip Search Results
        * @since 2.0
        */
        
        if (is_page_template('trip-search-results.php')) {
          wp_enqueue_script(
            'trip-search-results',
            get_stylesheet_directory_uri() . '/js/min/trip-search-results-min.js',
            false,
            false,
            true
          );
        }
        

	}
	add_action( 'wp_enqueue_scripts', 'ns_scripts_method' );
}


/*
* GRAVITY FORMS ADMIN
*
* Enables the option to hide field labels in the form builder.
*/

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*
* FORCE VISUAL COMPOSER TO LOAD
*
* Load Visual Composer on Custom Post Types where the standard WYSIWYG is not used
*/

add_action( 'wp_enqueue_scripts', 'add_vc_stylesheet', 99 );

function add_vc_stylesheet() {
    wp_enqueue_style( 'js_composer_front' );
    wp_enqueue_style( 'js_composer_custom_css' );
    wp_enqueue_script( 'wpb_composer_front_js' );
}

/*
* CUSTOM ADMIN STYLES
*
* Loads Stylesheet for Custom Admin Styles and custom post type icons
*/

function load_custom_admin_styles() {
    wp_register_style( 'custom_dashicons', get_stylesheet_directory_uri() . '/css/admin/dashicons-styles.min.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_dashicons' );
    wp_enqueue_style( 'usarc_admin_styles' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_admin_styles' );

/*
* CONDITIONAL LOAD STYLESHEETS FOR CUSTOM POST TYPES
*/

function template_styles() {
  // Single Cruise Pages
  if(is_singular( array('cruise', 'train', 'combo') )){
    wp_enqueue_style('travel-mode-styles', get_stylesheet_directory_uri() .'/css/travel-mode/travel-mode.min.css');
  }

}
add_action('wp_enqueue_scripts', 'template_styles',20);


/*
* CUSTOM ACF COLUMNS - ADD FIELD
*/

function acf_field_group_columns($columns) {
    $columns['menu_order'] = __('Order');
    return $columns;
  } // end function reference_columns
  add_filter('manage_edit-acf-field-group_columns', 'acf_field_group_columns', 20);

  function acf_field_group_columns_content($column, $post_id) {
    switch ($column) {
      case 'menu_order':
        global $post;
        echo $post->menu_order;
        break;
      default:
        break;
    } // end switch
  } // end function reference_columns_content
  add_action('manage_acf-field-group_posts_custom_column', 'acf_field_group_columns_content', 20, 2);

// add default image setting to ACF image fields
// let's you select a defualt image
// this is simply taking advantage of a field setting that already exists
add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
function add_default_value_to_image_field($field) {
  $args = array(
    'label' => 'Default Image',
    'instructions' => 'Appears when creating a new post',
    'type' => 'image',
    'name' => 'default_value'
  );
  acf_render_field_setting($field, $args);
}

//Enqueue WP Media Scripts on ACF Field Group Edit Page
add_action('admin_enqueue_scripts', 'enqueue_uploader_for_image_default');
function enqueue_uploader_for_image_default() {
  $screen = get_current_screen();
  if ($screen && $screen->id && ($screen->id == 'acf-field-group')) {
    acf_enqueue_uploader();
  }
}

//Reset Default image
add_filter('acf/load_value/type=image', 'reset_default_image', 10, 3);
function reset_default_image($value, $post_id, $field) {
  if (!$value) {
    $value = $field['default_value'];
  }
  return $value;
}

/*
* CUSTOM COLUMNS FOR CRUISES
*/

// Modify/Add Columns

add_filter( 'manage_cruise_posts_columns', 'set_custom_edit_cruise_columns' );

function set_custom_edit_cruise_columns( $columns ) {

  $columns['title'] = __( 'Cruise Name');
  $columns['cruise_ship'] = __( 'Cruise Ship');
  $columns['id'] = __('ID');

  return $columns;
}

add_action( 'manage_cruise_posts_custom_column' , 'custom_cruise_column', 10, 2 );

function custom_cruise_column( $column, $post_id ) {

	$ship = get_field('cruise_ship', $post_id );
	$output = '';

  switch ( $column ) {

    // display the value of Cruise Ship - ACF (Advanced Custom Fields) field
    case 'cruise_ship' :

    	if($ship){
    		foreach( $ship as $s ): // variable must NOT be called $post (IMPORTANT)
					$output .= get_the_title( $s->ID );
				endforeach;
    	}

      case 'id' : {
        echo $post_id;
      }

      break;

  }
  echo $output;
}

add_filter( 'manage_train_posts_columns', 'set_custom_edit_train_columns' );

function set_custom_edit_train_columns( $columns ) {
  $columns['id'] = __('ID');
  return $columns;
}

add_action( 'manage_train_posts_custom_column' , 'custom_train_column', 10, 2 );

function custom_train_column( $column, $post_id ) {
  switch ( $column ) {
      case 'id' : {
        echo $post_id;
      }
      break;
  }
}


 /*
 * Use Display Post Shortcode with Cruise Cards
 */
  add_filter( 'display_posts_shortcode_output', 'be_display_posts_custom_fields', 10, 1);
  function be_display_posts_custom_fields( $output ) {
      // Get our custom fields
     global $post;
     ob_start();
     include get_stylesheet_directory().'/php-partials/usarc_single_card.php';
     $output = ob_get_clean();
     return $output;
  }

function be_display_posts_open( $output) {
	$output = '<section id="cruise-archive" class="display-posts-cards">
      <div class="cruise-archive-content grid-container">';
	return $output;
}
add_filter( 'display_posts_shortcode_wrapper_open', 'be_display_posts_open' );

function be_display_posts_close( $output) {
	$output = '</div></section>';
	return $output;
}
add_filter( 'display_posts_shortcode_wrapper_close', 'be_display_posts_close' );

//Rewrite URLs for "testimonial" category
add_filter( 'post_link', 'custom_permalink', 10, 3 );
function custom_permalink( $permalink, $post, $leavename ) {
    // Get the category for the post
    $category = get_the_category($post->ID);
    if (  !empty($category) && $category[0]->cat_name == "Testimonials" ) {
        $cat_name = strtolower($category[0]->cat_name);
        $permalink = trailingslashit( home_url('/'. $cat_name . '/' . $post->post_name .'/' ) );
    }
    return $permalink;
}

add_filter( 'category_link', 'custom_category_permalink', 10, 2 );
function custom_category_permalink( $link, $cat_id ) {
    $slug = get_term_field( 'slug', $cat_id, 'category' );
    if ( ! is_wp_error( $slug ) && 'testimonials' === $slug ) {
        $link = home_url( user_trailingslashit( '/testimonials/', 'category' ) );
    }
    return $link;
}

add_action( 'init', 'custom_rewrite_rules' );
function custom_rewrite_rules() {
    add_rewrite_rule(
        'testimonials(?:/page/?([0-9]{1,})|)/?$',
        'index.php?category_name=testimonials&paged=$matches[1]',
        'top' // The rule position; either 'top' or 'bottom' (default).
    );
    add_rewrite_rule(
        'testimonials/([^/]+)(?:/([0-9]+))?/?$',
        'index.php?category_name=testimonials&name=$matches[1]&page=$matches[2]',
        'top' // The rule position; either 'top' or 'bottom' (default).
    );
}

add_filter('wpseo_canonical', 'filter_wf_cruise');

function filter_wf_cruise ($canonical) {

	// Target only cruise post types
	if ( (get_post_type($post) == 'cruise')) {

		//If URL ends with some sort of "-x" where x is an integer, snip it off and replace the canonical
		if (preg_match('/\-\d+\/$/', $canonical, $matches)){

			$canonical = str_replace($matches[0], '/', $canonical);

		};

	}

	return $canonical;
}


//Get Cruise title for booking 
add_filter( 'gform_field_value_cruise_id', 'wf_populate_cruise_name' );
function wf_populate_cruise_name($value){
  if ($value) {
    return get_the_title((int)$value);
  }
  else {
    wp_redirect(get_site_url('', '/cruise/'));
    exit;
  }
}

//Function to carry over cruise booking dates to form
//Update form ID in the future accordingly
add_filter( 'gform_pre_render_14', 'wf_populate_cruise_dates' );
add_filter( 'gform_pre_validation_14', 'wf_populate_cruise_dates' );
add_filter( 'gform_pre_submission_filter_14', 'wf_populate_cruise_dates' );
add_filter( 'gform_admin_pre_render_14', 'wf_populate_cruise_dates' );

// @todo I bet we can get this with the cruise slug, instead of individual parameters
function wf_populate_cruise_dates( $form ) {
  //Get the cruise object
  if ($_GET['cruise_id']){
    $cruise_object = (int)$_GET['cruise_id'];
  }

  //The dates from the cruise object
  $cruise_dates = get_field('available_dates', $cruise_object);

  //The cabins from the cruise object
  $cruise_cabins = get_field('cabin_details', $cruise_object);

  //The choices the user will be able to select
  //Define this outside of the foreach loop so that it doesn't keep getting reset
  //Dates
  $date_choices = array();

  if ($cruise_dates){
  
    foreach ($cruise_dates as $cruise_date){
      $date_choices[] = array( 'text' => $cruise_date['date'], 'value' => $cruise_date['date'] );
    }

  }

  //Cabins
  $cabin_choices = array();

  if ($cruise_cabins){
  
    foreach ($cruise_cabins as $cruise_cabin){
      $cabin_choices[] = array( 'text' => $cruise_cabin['cabin_name'], 'value' => $cruise_cabin['cabin_name'] );
    }

  }
  
  $loop_working = 0;
  //Loop through each field to find select fields
  foreach ( $form['fields'] as &$field ) {

    //Here's where the logic is. We need to check for each field's class so we can populate info accordingly
      //First the dates
    if ( $field->type == 'select' && strpos( $field->cssClass, 'available-dates' ) !== false) {
      $field->placeholder = "Select a Date";
      $field->choices = $date_choices;
    } 
    if ( $field->type == 'select' && strpos( $field->cssClass, 'cabin-details' ) !== false) {
      $field->placeholder = "Select a Cabin Category";
      $field->choices = $cabin_choices;
    }  
  }
  return $form;
}

// update '1' to the ID of your form
add_filter( 'gform_pre_render_14', 'add_readonly_script' );
function add_readonly_script( $form ) { ?>
 
    <script type="text/javascript">
        jQuery(document).ready(function(){
            /* apply only to a textarea with a class of gf_readonly */
            jQuery("li.gf_readonly input").attr("readonly","readonly");
        });
    </script>
 
    <?php
    return $form;
}

//Add Alert Bar options
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Alert Bar Settings',
		'menu_title'	=> 'Alert Bar',
		'menu_slug' 	=> 'alert-bar-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
  ));
}

//Referral URL for form
add_filter( 'gform_field_value_refurl', 'populate_referral_url');
 
function populate_referral_url( $form ){
    // Grab URL from HTTP Server Var and put it into a variable
    $refurl = $_SERVER['HTTP_REFERER'];
 
    // Return that value to the form
    return esc_url_raw($refurl);
}