<?php
/**
 * Template Name: Trip Search Results
 *
 * @package WordPress
 * @subpackage Jupter
 * @since USA River Cruises 1.0
 */

$items = array();

if (isset($_GET['submit'])){
  foreach ($_GET as $key => $value) {
    $items[$key] = $value;
  };
}

get_header();

$current_year = date('Y'); // get current year
$current_year_time = strtotime($current_year); // convert current year to time
$next_year = date('Y', strtotime('+1 year')); // get next year
$next_year_time = strtotime($next_year); // convert next year to time
$todays_date = date('F j, Y'); // get date in ACF output format
$todays_date_time = strtotime($todays_date); // convert date string to time


// Define Args
$display_count = 16;
$page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$offset = ($page - 1) * $display_count;
$args = array(
  'posts_per_page' => $display_count,
  'page'           => $page,
  'offset'         => $offset,
);

// Define Array for meta_query
$meta_queries = array();

//Define Post Type to use in query
if (!empty($items['travel-type']) && $items['travel-type'] != 'cruise_train'){
  $post_type = array($items['travel-type']);
}

//Search radio from search-block shortcode
//Specify post type if "Ship" or "Rail" is checked
elseif (!empty($items['search-radio'])){
    if($items['search-radio'] == 'rail'){
        $post_type=array('train');
    }
    elseif($items['search-radio'] == 'ships'){
        $post_type=array('cruise');
    }
}

// Commenting this out because we're replacing this param with 'destination'
// elseif (!empty($items['rail'])){
//   $post_type=array('train');
// }
else {
  $post_type = array('cruise', 'train');
}

if (!empty($items['ships'])){
  //commenting this out for now because it's redundant with search-radio param
  //$post_type = 'cruise';
  $meta_queries[] = array(
    'key'     => 'cruise_ship', // name of custom field
		'value'   => '"' . $items['ships'] . '"',
		'compare' => 'LIKE',
  );
}

//Define Destinations
if (!empty($items['destination'])){
  $destination = array($items['destination']);
  $args['tax_query'] = array( //pull from Destinations
      array(
        'taxonomy' => 'destination',
        'field'    => 'slug',
        'terms'    => $destination,
      ),
    );
}

//Define Destinations for Rails
elseif (!empty($items['rail'])){
  $destination = array($items['rail']);
  $args['tax_query'] = array( //pull from Destinations
      array(
        'taxonomy' => 'destination',
        'field'    => 'slug',
        'terms'    => $destination,
      ),
    );
}
else {
  $terms = get_terms('destination');
  $destination = array();
  foreach($terms as $term){
    $destination[] = $term->slug;
  }
  $args['tax_query'] = array( //pull from Destinations
      array(
        'taxonomy' => 'destination',
        'field'    => 'slug',
        'terms'    => $destination,
      ),
    );
}

$args['post_type'] = $post_type;

//Cruise Ship
if(!empty($items['length'])){
  $meta_queries[] = array(
    'key'     => 'tm_length_days',
    'value'   => (int)$items['length'],
    'type'		=> 'NUMERIC',
    'compare' => '<=',
  );
}

//Cruise Length
if(!empty($items['ship'])){
  $meta_queries[] = array(
    'key'     => 'tm_length_days',
    'value'   => (int)$items['length'],
    'type'		=> 'NUMERIC',
    'compare' => '<=',
  );
}

//Cruise Max Price
if(!empty($items['price'])){
  $meta_queries[] = array(
    'key'     => 'double_occupancy',
    'value'   => $items['price'],
    'type'		=> 'NUMERIC',
    'compare' => '<=',
  );
}

// Filter for Month/Date subfields for ACF
function available_dates_replacer( $where ) {
  global $wpdb;
  $where = str_replace("meta_key = 'available_dates_%", "meta_key LIKE 'available_dates_%", $wpdb->remove_placeholder_escape($where));
	return $where;
}

add_filter('posts_where', 'available_dates_replacer');

//Cruise Month
if(!empty($items['month'])){
  if (!empty($items['cruise-year'])){

    //start month
    $start_date = $items['cruise-year'] . $items['month'] . '01';

    //get last date of month
    $end_date = date('Ymt', strtotime($start_date));

    $meta_queries[] = array(
      'key'     => 'available_dates_%_date',
      'value'   => array($start_date, $end_date),
      'compare' => 'BETWEEN',
      'type'    => 'DATE',
    );
  }

  else {
    $current_year = date("Y");
    $future_year = $current_year + 2;
    $usarc_search_query_dates = array(
      'relation' => 'OR',
    );
    while ($future_year >= $current_year){

      //start month
      $start_date = $current_year . $items['month'] . '01';
      //get last date of month
      $end_date = date('Ymt', strtotime($start_date));

      //Create new query for selected month with each year
      $usarc_search_query_dates[] = array(
        'key'     => 'available_dates_%_date',
        'value'   => array($start_date, $end_date),
        'compare' => 'BETWEEN',
        'type'    => 'DATE',
      );
      $current_year++;
    }

    $meta_queries[] = $usarc_search_query_dates;
  }
}
elseif (!empty($items['cruise-year'])){
  $meta_queries[] = array(
    'key'     => 'available_dates_%_date',
    'value'   => array($items['cruise-year'] . '0101', $items['cruise-year'] . '1231'),
    'compare' => 'BETWEEN',
    'type'    => 'DATE',
  );
}

elseif (!empty($items['monthyear'])){
  $monthyear = str_replace('/','-',$items['monthyear']);
  $start_date = date('Ymd', strtotime('01-' . $monthyear));
  //get last date of month
  $end_date = date('Ymt', strtotime($start_date));
  $meta_queries[] = array(
    'key'     => 'available_dates_%_date',
    'value'   => array($start_date, $end_date),
    'compare' => 'BETWEEN',
    'type'    => 'DATE',
  );
}

//Sidebar Search
elseif (!empty($items['sidebar-cruise-search'])){
  $sidebar_search = $items['sidebar-cruise-search'];
  $acf_field_groups = acf_get_field_groups(array('post_type' => 'cruise'));
  $acf_fields;
  foreach($acf_field_groups as $key => $value){
    $acf_fields[] = acf_get_fields($value['key']);
  }
  $acf_field_groups = acf_get_field_groups(array('post_type' => 'train'));
  foreach($acf_field_groups as $key => $value){
    $acf_fields[] = acf_get_fields($value['key']);
  }
  foreach($acf_fields as $key => $value){
    $meta_queries[] = array (
      'key'     => $value[0]['name'],
      'value'   => $sidebar_search,
      'compare' => 'LIKE',
    );
  }
  $meta_queries['relation'] = 'OR';
}

// Create Meta Queries
if (!empty($meta_queries)){
  $args['meta_query'] = $meta_queries;
};

$usarc_search_query = new WP_Query($args);

// Pagination fix
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $usarc_search_query;

include(get_stylesheet_directory() . '/php-partials/usarc_cards.php');

// Reset main query object
$wp_query = NULL;
$wp_query = $temp_query;

get_footer();

?>
