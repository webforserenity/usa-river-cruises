function getUrlParams(search) {
    let hashes = search.slice(search.indexOf('?') + 1).split('&')
    let params = {}
    hashes.map(hash => {
        let [key, val] = hash.split('=')
        params[key] = decodeURIComponent(val)
    })

    let cruiseSearchForm = document.getElementsByClassName('cruise-finder-fields')[0];
    console.log(Object.keys(params));
    for (const [key, value] of Object.entries(params)) {
        console.log(('select[name="' + key.toString() + '"]'))
        document.querySelector('select[name="' + key.toString() + '"]').value = value.toString();
    }
}

getUrlParams(window.location.search);