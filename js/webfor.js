(function($){
  function setCardHeight(){
    $('.cruise-card-content').css('height', 'auto');
    if ($(window).width() > 767) {
      var cardHeight = 0;
      $('.cruise-card-content').each(function(){
        if ($(this).height() > cardHeight) {
          cardHeight = $(this).height();
        }
      });
      $('.cruise-card-content').height(cardHeight);
    }
  }
  var paddingHeight = $('.mk-header-padding-wrapper').outerHeight();
  function paddingAdjust(){
    var searchHeight = $('#usarc-search').height();
    $('.mk-header-padding-wrapper').css('padding-top', searchHeight + paddingHeight);
  }
  $(document).ready(function(){
    paddingAdjust();
    setCardHeight();
  });
  $(window).resize(function(){
    setCardHeight();
    paddingAdjust();
  })

	//Shortcode Scripts//
	$('form#search-icons select').change(function(){
      $(this).css("color", "#000");
  });
  $('#monthpicker').MonthPicker({ Button: false });

  if($('body').hasClass('page-id-2')){
  	// Reset form for 'helpful' browsers
  	$(document).ready(function(){
        $('#search-icons')[0].reset();
    });
    $('input[type="radio"][name="search-radio"]').change(function(){
        var type = $(this).attr('value');
        $('#search-icons option:selected').removeAttr('selected');
        $('#search-icons select').hide();
        $('#search-icons select').val($('#search-icons select option:first').val());
        $('#search-icons select').css("color", "#9b9b9b");
        $('#search-icons select[name="' + type + '"]').show();
    });
  }

  // Travel Mode Header Card Events

  $('#card--more-dates').click(function(event) {
  	$('#card--hidden-dates').toggle();
  });

  // Add Classes for Marketing Content

  var marketingContent = $('#content--marketing');

  var total = $(marketingContent).find('.vc_row').length;

  $(marketingContent).find('.vc_row').each(function(i) {
  	$(this).find('.vc_col-sm-6').each(function(j) {
  		if(j == 0){
  			$(this).addClass('first_col')
  		}
  		if(j == 1){
  			$(this).addClass('second_col')
  		}
  	});
  	if(i == 0){
  		$(this).addClass('first_row');
  	}
  	if(i % 2 == 0){
  		$(this).addClass('even_row');
  	}
  	else{
  		$(this).addClass('odd_row');
  	}
  	if (i === total - 1) {
        $(this).addClass('last_row');
    }
  });

  //Dynamically adjust bottom margin of header for search bar container
  function searchBarHeightAdjust(){
    var searchHeight = $('#search-container').outerHeight()
    $('.mk-header-padding-wrapper').css('margin-bottom', searchHeight+'px')
  }
  $(document).ready(searchBarHeightAdjust);
  $(window).resize(searchBarHeightAdjust);

})(jQuery);
