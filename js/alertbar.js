(function($){
    
    let barHeight;
    function alertBarAdjust(){
        if ($('.alert-bar').length ) {
            barHeight = $('.alert-bar').outerHeight();
            if ($(window).width() > 1024 ) {
                $('header .mk-header-holder').css('margin-top', barHeight + 'px');
            }
            $('.sfm-navicon-button ').css('top', barHeight + 15 + 'px');
        }
    }
    $(window).on('load', alertBarAdjust());
    $(window).resize(alertBarAdjust());
    $(window).scroll(function(){
        if ($(window).scrollTop() > barHeight){
            $('.mk-header-holder').css('margin-top', '0px');
        }
        else {
            alertBarAdjust();
        }
    })
})(jQuery);