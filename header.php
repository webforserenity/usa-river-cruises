<!DOCTYPE html>
<html <?php echo language_attributes();?> >
<head>
    <?php wp_head(); ?>
	<meta name="google-site-verification" content="SdDDuJKGbTZYRXyPtbN8_w1vnF2C0Iuc_6Ln4xHa1Vg" />
</head>

<body <?php body_class(mk_get_body_class(global_get_post_id())); ?> <?php echo get_schema_markup('body'); ?> data-adminbar="<?php echo is_admin_bar_showing() ?>">
	
	<script type="text/javascript">
		var _mfq = _mfq || [];
		(function() {
			var mf = document.createElement("script");
			mf.type = "text/javascript"; mf.async = true;
			mf.src = "https://cdn.mouseflow.com/projects/fc840a8e-5266-479a-8d43-1065200ba216.js";
			document.getElementsByTagName("head")[0].appendChild(mf);
		})();
	</script>
	<?php
		// Hook when you need to add content right after body opening tag. to be used in child themes or customisations.
		do_action('theme_after_body_tag_start');
	?>

	<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
	<div id="top-of-page"></div>

		<div id="mk-boxed-layout">

			<?php if(get_field('alert_is_enabled', 'option' ) ) : ?>
				<div class="alert-bar" style="color:<?php get_field('alert_text_color', 'option' )?>;background-color:<?php get_field('alert_background_color', 'option' )?>"><?php the_field('alert_text', 'option' )?></div>
			<?php endif ; ?>
			<div id="mk-theme-container" <?php echo is_header_transparent('class="trans-header"'); ?>>

				<?php mk_get_header_view('styles', 'header-'.get_header_style()); ?>
